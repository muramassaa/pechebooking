using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;
using PecheBooking.Data;

namespace PecheBooking.TagHelpers
{
    [HtmlTargetElement("configlink")]
    public class ConfigLinkHelper : TagHelper
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Class { get; set; }
        public string Target { get; set; }
        public ApplicationDbContext _context { get; set; }

        public ConfigLinkHelper(ApplicationDbContext context)
        {
            _context = context;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var item = _context.Configs.SingleOrDefault(r => r.Id == Id);
            var a = new TagBuilder("a");

            output.TagName = "";
            
            if (item.Value == null)
            {
                item.Value = "#";
            }
            switch(Type)
            {
                case "mail":
                    a.MergeAttribute("href", "mailto:"+item.Value);
                    a.InnerHtml.Append(item.Value);
                    break;

                default:
                    a.MergeAttribute("href", item.Value);
                    if (Class != null)
                    {
                        a.MergeAttribute("class", Class);
                    }
                    if (Target != null)
                    {
                        a.MergeAttribute("target", Target);
                    }
                    break;
            }
            output.Content.AppendHtml(a);
        }
    }
}
