using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;
using PecheBooking.Data;

namespace PecheBooking.TagHelpers
{
    [HtmlTargetElement("config")]
    public class ConfigTagHelper : TagHelper
    {
        public string Id { get; set; }
        public ApplicationDbContext _context { get; set; }

        public ConfigTagHelper(ApplicationDbContext context)
        {
            _context = context;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var item = _context.Configs.SingleOrDefault(r => r.Id == Id);
            output.TagName = "";
            if (item == null)
            {
                item.Value = "";
            }
            output.Content.SetContent(item.Value);
        }
    }
}
