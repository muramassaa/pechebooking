using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;
using PecheBooking.ViewModels.ReserveViewModels;

namespace PecheBooking.Controllers
{
    public class ReserveController : Controller
    {
        public CultureInfo Culture { get; set; }
        public ApplicationDbContext Context { get; set; }

        public ReserveController(ApplicationDbContext context)
        {
            Context = context;
            Culture = CultureInfo.CurrentCulture;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(InformationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = Context.Reservations.Add(new ApplicationReservation {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber,
                    IPAdress = HttpContext.Connection.RemoteIpAddress?.ToString(),
                    ReservationInfos = new List<ApplicationReservationInfo>()
                });
                for(var date = model.Start.Value; date <= model.End; date = date.AddDays(1))
                {
                    result.Entity.ReservationInfos.Add(new ApplicationReservationInfo() {
                        Date = date,
                        Slot = model.Slot
                    });
                }
                await Context.SaveChangesAsync();
                return RedirectToAction("ReservationConfirm", model);
            }
            return View(model);
        }
        
        [HttpGet]
        public IActionResult ReservationConfirm(InformationViewModel model)
        {
            if (ModelState.IsValid)
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }

        public IActionResult DateValidation(DateTime Start, DateTime End)
        {
            var Today = DateTime.Today;
            var StartWeek = Culture.Calendar.GetWeekOfYear(Start, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var EndWeek = Culture.Calendar.GetWeekOfYear(End, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            //
            var test = Start.Subtract(End).TotalDays;
            if (Start.Subtract(Today).TotalDays < 0)
            {
                return Json("La période de réservation est expiré.");
            }
            else if (Start.Ticks > End.Ticks)
            {
                return Json("La période de réservation est incorrect.");
            }
            else if (End.Subtract(Start).TotalDays > 3)
            {
                return Json("La réservation ne peut excellé 3 jours consécutifs.");
            }
            else if (Today.Subtract(Start).TotalDays > 30 || Today.Subtract(End).TotalDays > 30)
            {
                return Json("La réservation ne peut être pris plus de 30 jours à l'avance.");
            }
            else if (StartWeek != EndWeek)
            {
                return Json("La réservation ne peut être pris sur deux semaines à la fois.");
            }
            else if (Context.Events.Any(r => r.StartDate >= Start && r.StartDate <= End && r.EndDate <= End))
            {
                return Json("La réservation ne peut contenir un jour d'évévement.");
            }
            else if (Context.ReservationInfos
                        .GroupBy(r => r.Date)
                        .Where(r => r.Count() >= 2)
                        .Where(r => r.Key >= Start && r.Key <= End)
                        .Any())
            {
                return Json("La réservation ne peut contenir un jour complet.");
            }
            return Json(true);
        }

        public IActionResult SlotValidation(DateTime Start, DateTime End, int Slot)
        {
            var Week = Culture.Calendar.GetWeekOfYear(Start, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            var WeekPair = ((Week % 2) == 0);
            var SlotPair = ((Slot % 2) == 0);
            if (SlotPair != WeekPair)
            {
                if (WeekPair)
                    return Json("Veuillez selectionner un poste paire.");
                else
                    return Json("Veuillez selectionner un poste impaire.");
            }
            if (Context.ReservationInfos.Count(r => r.Date >= Start && r.Date <= End && r.Slot == Slot) > 0)
            {
                return Json("Ce poste est déjà réservé durant cette période.");
            }
            return Json(true);
        }
    }
}