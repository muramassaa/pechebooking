using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Data;
using PecheBooking.ViewModels.HomeViewModels;

namespace PecheBooking.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        
        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        [HttpGet]
        public async Task<IActionResult> _MenuPartial()
        {
            return PartialView(await _context.Menus.OrderBy(r => r.MenuText).ToListAsync());
        }

        [HttpGet]
        public IActionResult Credit()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Gallery(int? id, GalleryViewModel model)
        {
            model.Count = 12;
            model.Total = await _context.Galleries.CountAsync();
            model.PageCount = (model.Total / model.Count) + (model.Total % model.Count > 0 ? 1 : 0);
            model.Page = id.HasValue ? id.Value <= model.PageCount ? id.Value : 1 : 1;
            model.Previous = model.Page > 1 ? model.Page - 1 : -1;
            model.Next = model.Page < model.PageCount ? model.Page + 1 : -1;
            model.Offset = (model.Count * model.Page) - model.Count;
            model.Images = await _context.Galleries
                .OrderBy(r => r.Id)
                .Skip(model.Offset)
                .Take(model.Count)
                .ToListAsync();
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Page(int id)
        {
            var model = await _context.Pages.SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Error()
        {
            return View();
        }
    }
}
