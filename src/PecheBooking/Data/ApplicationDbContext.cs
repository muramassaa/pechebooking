using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;

namespace PecheBooking.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<ApplicationConfig> Configs { get; set; }
        public DbSet<ApplicationEvent> Events { get; set; }
        public DbSet<ApplicationImage> Galleries { get; set; }
        public DbSet<ApplicationMenu> Menus { get; set; }
        public DbSet<ApplicationPage> Pages { get; set; }
        public DbSet<ApplicationReservation> Reservations { get; set; }
        public DbSet<ApplicationReservationInfo> ReservationInfos { get; set; }
    }
}
