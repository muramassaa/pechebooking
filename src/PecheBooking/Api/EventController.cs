using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PecheBooking.Data;

namespace PecheBooking.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class EventController : Controller
    {
        public ApplicationDbContext Context { get; set; }

        public EventController(ApplicationDbContext context)
        {
            Context = context;
        }

        [HttpGet("")]
        public IEnumerable<object> GetEvent()
        {
            return Context.Events
                        .Where(r => r.EndDate >= DateTime.Today)
                        .Select(r => new
                        {
                            start = r.StartDate,
                            end = r.EndDate,
                            data = new { message = r.Description } 
                        });
        }
    }
}