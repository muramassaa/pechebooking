using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PecheBooking.Data;

namespace PecheBooking.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ReservationController : Controller
    {
        public ApplicationDbContext Context { get; set; }

        public ReservationController(ApplicationDbContext context)
        {
            Context = context;
        }

        [HttpGet("")]
        public IEnumerable<object> GetReservationFull()
        {
            return Context.ReservationInfos
                .GroupBy(r => r.Date)
                .Where(r => r.Count() >= 2 && r.Key >= DateTime.Today)
                .Select(r => new {
                    date = r.Key
                });
        }
    }
}