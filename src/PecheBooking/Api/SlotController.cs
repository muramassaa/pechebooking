using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PecheBooking.Data;

namespace PecheBooking.Api
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SlotController : Controller
    {
        public ApplicationDbContext Context { get; set; }

        public SlotController(ApplicationDbContext context)
        {
            Context = context;
        }

        public IEnumerable<int> GetSlotFull(string start, string end)
        {
            return Context.ReservationInfos
                .Where(r => r.Date >= DateTime.Parse(start) && r.Date <= DateTime.Parse(end))
                .Select(r => r.Slot);
        }
    }
}