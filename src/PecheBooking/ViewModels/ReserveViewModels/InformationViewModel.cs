using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace PecheBooking.ViewModels.ReserveViewModels
{
    public class InformationViewModel
    {
        [Required(ErrorMessage = "Le champs date de début est requis")]
        public DateTime? Start { get; set; }
        [Required(ErrorMessage = "Le champs date de fin est requis")]
        [Remote("DateValidation", "Reserve", AdditionalFields = "Start")]
        public DateTime? End { get; set; }
        [Range(1, 8)]
        [Remote("SlotValidation", "Reserve", AdditionalFields = "Start,End")]
        [Required(ErrorMessage = "Le champs poste est requis")]
        public int Slot { get; set; }
        [Required(ErrorMessage = "Le champs prénom est requis")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Le champs nom est requis")]
        public string LastName { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Le champs téléphone est requis")]
        public string PhoneNumber { get; set; }
    }
}