using System.Collections.Generic;
using PecheBooking.Areas.Admin.Models;

namespace PecheBooking.ViewModels.HomeViewModels
{
    public class GalleryViewModel
    {
        public int Count { get; set; }
        public int Offset { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public int Previous { get; set; }
        public int Next { get; set; }
        public int Total { get; set; }
        public List<ApplicationImage> Images { get; set; }
    }
}