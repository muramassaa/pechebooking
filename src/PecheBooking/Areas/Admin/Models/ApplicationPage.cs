using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationPage
    {
        [Key]
        public int Id { get; set; }
        [MinLength(1)]
        [Display(Name="Nom")]
        [Required]
        public string Name { get; set; }
        [MinLength(1)]
        [Display(Name="Contenue")]
        [Required]
        public string Content { get; set; }
    }
}