using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationReservation
    {
        public int Id { get; set; }
        [Display(Name="Adresse IP")]
        public string IPAdress { get; set; }
        [Display(Name="Prénom")]
        [Required]
        public string FirstName { get; set; }
        [Display(Name="Nom")]
        [Required]
        public string LastName { get; set; }
        [Display(Name="Téléphone")]

        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public List<ApplicationReservationInfo> ReservationInfos { get; set; }
    }
}