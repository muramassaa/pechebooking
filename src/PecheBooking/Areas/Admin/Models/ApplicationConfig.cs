using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationConfig
    {
        [Key]
        [Required]
        public string Id { get; set; }
        [Required]
        public string Action { get; set; }
        [Required]
        public string Name { get; set; }
        public string Value { get; set; }
    }
}