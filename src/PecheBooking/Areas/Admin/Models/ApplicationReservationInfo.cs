using System;
using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationReservationInfo
    {
        public int Id { get; set; }
        public ApplicationReservation ApplicationReservation { get; set; }
        [Display(Name="Poste")]
        [Required]
        public int Slot { get; set; }
        [Required]
        public DateTime Date { get; set; }
    }
}