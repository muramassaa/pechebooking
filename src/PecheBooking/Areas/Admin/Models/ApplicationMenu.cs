using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationMenu
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Menu")]
        [Required]
        public string MenuText { get; set; }
        [Display(Name = "Lien")]
        [Required]
        public string LinkUrl { get; set; }
    }
}