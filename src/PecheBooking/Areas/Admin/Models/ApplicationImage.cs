using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationImage
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        public string ThumbnailImage { get; set; }
    }
}