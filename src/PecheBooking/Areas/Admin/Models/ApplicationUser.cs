using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PecheBooking.Areas.Admin.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
    }
}
