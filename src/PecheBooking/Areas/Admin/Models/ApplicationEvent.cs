using System;
using System.ComponentModel.DataAnnotations;

namespace PecheBooking.Areas.Admin.Models
{
    public class ApplicationEvent
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Display(Name="Départ")]
        [Required]
        public DateTime StartDate { get; set; }
        [Display(Name="Fin")]
        [Required]
        public DateTime EndDate { get; set; }
    }
}