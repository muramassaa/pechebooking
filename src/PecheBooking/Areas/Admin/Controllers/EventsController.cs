using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class EventsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EventsController(ApplicationDbContext context)
        {
            _context = context;
        }

        //
        // GET: /Events/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Events/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id, Description, EndDate, StartDate")] ApplicationEvent applicationEvent)
        {
            if (ModelState.IsValid)
            {
                if (applicationEvent.StartDate > applicationEvent.EndDate)
                {
                    ModelState.AddModelError(string.Empty, "La période est incorrect.");
                    return View(applicationEvent);
                }
                _context.Add(applicationEvent);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Reservations", new { Message = ReservationsController.ReservationMessageId.EventCreate });
            }
            return View(applicationEvent);
        }

        //
        // GET /Events/Edit/1
        [HttpGet]        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var model = await _context.Events.SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        //
        // POST : /Events/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Description, EndDate, StartDate")] ApplicationEvent applicationEvent)
        {
            if (id != applicationEvent.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (applicationEvent.StartDate > applicationEvent.EndDate)
                {
                    ModelState.AddModelError(string.Empty, "La période est incorrect.");
                    return View(applicationEvent);
                }
                try
                {
                    _context.Update(applicationEvent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationEventExists(applicationEvent.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction("Index", "Reservations", new { Message = ReservationsController.ReservationMessageId.EventEdit });
        }

        //
        // GET : /Events/Delete/1
        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationEvent = await _context.Events.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationEvent == null)
            {
                return NotFound();
            }
            return View(applicationEvent);
        }

        //
        // POST : //Events/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var applicationEvent = await _context.Events.SingleOrDefaultAsync(m => m.Id == id);
            _context.Events.Remove(applicationEvent);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", "Reservations", new { Message = ReservationsController.ReservationMessageId.EventDelete });
        }

        private bool ApplicationEventExists(int id)
        {
            return _context.Events.Any(e => e.Id == id);
        }
    }
}