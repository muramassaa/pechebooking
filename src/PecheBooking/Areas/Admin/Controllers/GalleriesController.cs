using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize]
    public class GalleriesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private IHostingEnvironment _environment;

        public GalleriesController(
            ApplicationDbContext context,
            IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        //
        // GET /Galleries/Index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Galleries.OrderBy(r => r.Id).ToListAsync());
        }

        //
        // GET : /Galleries/Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        //
        // GET : /Galleries/Delete/1
        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationImage = await _context.Galleries.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationImage == null)
            {
                return NotFound();
            }
            return View(applicationImage);
        }

        //
        // POST : //Galleries/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var applicationImage = await _context.Galleries.SingleOrDefaultAsync(m => m.Id == id);
            _context.Galleries.Remove(applicationImage);
            System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", applicationImage.Image));
            System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", applicationImage.ThumbnailImage));
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        //
        // GET /Galleries/Edit/1
        [HttpGet]        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var model = await _context.Galleries.SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        //
        // POST : /Menu/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(int id, IFormFile image)
        {
            if (ModelState.IsValid)
            { 
                var applicationImage = await _context.Galleries.SingleOrDefaultAsync(r => r.Id == id);

                if (id != applicationImage.Id)
                {
                    return NotFound();
                }

                if (applicationImage == null)
                {
                    return NotFound();
                }
                if (image == null)
                {
                    return NotFound();
                }
                try
                {
                    applicationImage = await CreateImage(image, applicationImage);
                    if (applicationImage == null)
                    {
                        return BadRequest();
                    }
                    _context.Update(applicationImage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationImageExists(applicationImage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction("Index");
        }

        //
        // GET /Galleries/Upload
        [HttpPost]
        public async Task<JsonResult> Upload(IFormFile file)
        {
            if (ModelState.IsValid)
            {
                var applicationImage = await CreateImage(file, new ApplicationImage());
                if (applicationImage == null)
                {
                    return Json("Error");
                }
                _context.Add(applicationImage);
                await _context.SaveChangesAsync();

            }
            return Json("Ok");
        }

        #region Helper

        private async Task<ApplicationImage> CreateImage(IFormFile file, ApplicationImage applicationImage)
        {
            try
            {
                var acceptType = new string[] { "image/jpeg", "image/png", "image/bmp" };
                var filename = Path.GetFileNameWithoutExtension(file.FileName);
                var uploads = Path.Combine(_environment.WebRootPath, "uploads");
                var uniqid = Guid.NewGuid().ToString();

                if (!Directory.Exists(uploads))
                {
                    Directory.CreateDirectory(uploads);
                }
                if (!acceptType.Any(r => r == file.ContentType))
                {
                    return null;
                }
                if (file.Length == 0)
                {
                    return null;
                }
                if (applicationImage.Image != null)
                {
                    System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", applicationImage.Image));
                    System.IO.File.Delete(Path.Combine(_environment.WebRootPath, "uploads", applicationImage.ThumbnailImage));
                }

                applicationImage.Image = file.FileName.Replace(filename, uniqid);
                applicationImage.ThumbnailImage = file.FileName.Replace(filename, uniqid+".thumb");

                using (var fileStream = new FileStream(Path.Combine(uploads, applicationImage.Image), FileMode.CreateNew))
                {
                    await file.CopyToAsync(fileStream);
                }

                Image.FromFile(Path.Combine(uploads, applicationImage.Image))
                    .GetThumbnailImage(250, 180, null, IntPtr.Zero)
                    .Save(Path.Combine(uploads, applicationImage.ThumbnailImage));
            }
            catch (Exception)
            {
                return null;
            }
            return applicationImage;
        }

        #endregion

        private bool ApplicationImageExists(int id)
        {
            return _context.Menus.Any(e => e.Id == id);
        }
    }
}