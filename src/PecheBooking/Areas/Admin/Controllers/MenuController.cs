using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class MenusController : Controller
    {
        private readonly ApplicationDbContext _context;

        public MenusController(ApplicationDbContext context)
        {
            _context = context;
        }

        //
        // GET /Menu/Index
        [HttpGet]
        public async Task<IActionResult> Index(MenuMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == MenuMessageId.Create ? "Le menu a été crée avec succés."
                : message == MenuMessageId.Edit ? "Le menu a été modifié avec succés."
                : message == MenuMessageId.Delete ? "Le menu a été supprimé avec succés."
                : "";    
            return View(await _context.Menus.OrderBy(r => r.MenuText).ToListAsync());
        }

        //
        // GET: /Menu/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Menu/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id, MenuText, LinkUrl")] ApplicationMenu applicationMenu)
        {
            if (ModelState.IsValid)
            {
                _context.Add(applicationMenu);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { Message = MenuMessageId.Create });
            }
            return View(applicationMenu);
        }

        //
        // GET /Menu/Edit/1
        [HttpGet]        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var model = await _context.Menus.SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        //
        // POST : /Menu/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, MenuText, LinkUrl")] ApplicationMenu applicationMenu)
        {
            if (id != applicationMenu.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicationMenu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationMenuExists(applicationMenu.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction("Index", new { Message = MenuMessageId.Edit });
        }

        //
        // GET : /Menu/Delete/1
        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationMenu = await _context.Menus.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationMenu == null)
            {
                return NotFound();
            }
            return View(applicationMenu);
        }

        //
        // POST : //Menu/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var applicationMenu = await _context.Menus.SingleOrDefaultAsync(m => m.Id == id);
            _context.Menus.Remove(applicationMenu);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { Message = MenuMessageId.Delete });
        }

        private bool ApplicationMenuExists(int id)
        {
            return _context.Menus.Any(e => e.Id == id);
        }

        public enum MenuMessageId
        {
            Create,
            Edit,
            Delete
        }
    }
}