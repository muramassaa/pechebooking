using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;
using static PecheBooking.Areas.Admin.Controllers.AccountController;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class HomeController : Controller
    {
        public ApplicationDbContext _context { get; set; }

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        //
        // GET: /Admin/Index
        [HttpGet]
        public IActionResult Index(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Votre mot de passe a été modifié avec succès."
                : message == ManageMessageId.ChangeConfigurationSuccess ? "Vos configurations ont été modifiés avec succès."
                : message == ManageMessageId.ChangeSocialSuccess ? "Vos liens des réseaux sociaux ont été modifiés avec succès."
                : "";
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Configuration()
        {
            var model = await _context.Configs
                .Where(r => r.Action == nameof(Configuration))
                .ToListAsync();
                
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Configuration(List<ApplicationConfig> model)
        {
            if (ModelState.IsValid)
            {
                if (model == null)
                {
                    return NotFound();
                }
                _context.Configs.UpdateRange(model);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangeConfigurationSuccess });
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Social()
        {
            var model = await _context.Configs
                .Where(r => r.Action == nameof(Social))
                .ToListAsync();
                
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Social(List<ApplicationConfig> model)
        {
            if (ModelState.IsValid)
            {
                if (model == null)
                {
                    return NotFound();
                }
                _context.Configs.UpdateRange(model);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangeSocialSuccess });
            }
            return View(model);
        }
    }
}