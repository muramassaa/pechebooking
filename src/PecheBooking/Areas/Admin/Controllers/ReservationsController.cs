using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class ReservationsController : Controller
    {
        public ApplicationDbContext _context { get; set; }

        public ReservationsController(ApplicationDbContext context)
        {
            _context = context;
        }

        //
        // GET /Reservation/Index
        [HttpGet]
        public IActionResult Index(ReservationMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ReservationMessageId.EventCreate ? "L'événement a été crée avec succés."
                : message == ReservationMessageId.EventEdit ? "L'événement a été modifié avec succés."
                : message == ReservationMessageId.EventDelete ? "L'événement a été supprimé avec succés."
                : message == ReservationMessageId.ReservationEdit ? "La réservation a été modifié avec succés."
                : message == ReservationMessageId.ReservationDelete ? "La réservation a été supprimé avec succés."
                : "";

            var Reservations = _context.Reservations
                                    .Include(r => r.ReservationInfos)
                                    .ToList()
                                    .Select(r => new {
                                        id = r.Id,
                                        title = r.FirstName + " " + r.LastName + " P:" + r.ReservationInfos[0].Slot,
                                        start = r.ReservationInfos.First().Date,
                                        end = r.ReservationInfos.Last().Date.AddDays(1),
                                        url = string.Format("/Admin/Reservations/Edit/{0}", r.Id),
                                        color = "#FF0000",
                                        allDay = true })
                                    .Where(r => r.end >= DateTime.Today);

            var Events = _context.Events
                                    .Select(r => new {
                                        id = r.Id,
                                        title = r.Description,
                                        start = r.StartDate,
                                        end = r.EndDate.AddDays(1),
                                        url = string.Format("/Admin/Events/Edit/{0}", r.Id),
                                        color = "#FFA500",
                                        allDay = true})
                                    .Where(r => r.end >= DateTime.Today);

            return View(Reservations.Union(Events));
        }

        //
        // GET /Reservation/Edit/1
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var model = await _context.Reservations.Include(r => r.ReservationInfos).SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        //
        // POST : /Reservation/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(
            int id, 
            [Bind("Id, IPAdress, FirstName, LastName, PhoneNumber, ReservationInfos")] ApplicationReservation applicationReservation, 
            [FromForm] DateTime start,
            [FromForm] DateTime end,
            [FromForm] int slot)
        {
            if (id != applicationReservation.Id)
            {
                return NotFound();
            }
            if (start > end)
            {
                ModelState.AddModelError(string.Empty, "La période est incorrect.");
                return View(applicationReservation);
            }
            try
            {
                if (applicationReservation.ReservationInfos.First().Date != start || 
                    applicationReservation.ReservationInfos.Last().Date != end)
                {
                    _context.ReservationInfos.RemoveRange(applicationReservation.ReservationInfos);
                    for(var date = start; date <= end; date = date.AddDays(1))
                    {
                        applicationReservation.ReservationInfos.Add(new ApplicationReservationInfo() {
                            Date = date,
                            Slot = slot
                        });
                    }
                }
                if (applicationReservation.ReservationInfos.First().Slot != slot)
                {
                    foreach (var item in applicationReservation.ReservationInfos)
                    {
                        item.Slot = slot;
                    }                    
                }
                _context.Update(applicationReservation);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationReservationExists(applicationReservation.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction("Index", new { Message = ReservationMessageId.ReservationEdit });
        }

        //
        // GET : /Reservation/Delete/1
        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationReservation = await _context.Reservations.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationReservation == null)
            {
                return NotFound();
            }
            return View(applicationReservation);
        }

        //
        // POST : //Events/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var applicationReservation = await _context.Reservations.Include(r => r.ReservationInfos).SingleOrDefaultAsync(m => m.Id == id);
            _context.ReservationInfos.RemoveRange(applicationReservation.ReservationInfos);
            _context.Reservations.Remove(applicationReservation);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { Message = ReservationMessageId.ReservationDelete });
        }

        private bool ApplicationReservationExists(int id)
        {
            return _context.Reservations.Any(e => e.Id == id);
        }

        public enum ReservationMessageId
        {
            EventCreate,
            EventEdit,
            EventDelete,
            ReservationEdit,
            ReservationDelete
        }
    }
}