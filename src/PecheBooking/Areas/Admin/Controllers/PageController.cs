using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    public class PagesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        //
        // GET /Page/Index
        [HttpGet]
        public async Task<IActionResult> Index(PageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == PageMessageId.Create ? "La page a été crée avec succés."
                : message == PageMessageId.Edit ? "La page a été modifié avec succés."
                : message == PageMessageId.Delete ? "La page a été supprimé avec succés."
                : "";    
            return View(await _context.Pages.ToListAsync());
        }

        //
        // GET: /Page/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Page/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id, Name, Content")] ApplicationPage applicationPage)
        {
            if (ModelState.IsValid)
            {
                if (ApplicationPageNameExists(applicationPage.Name))
                {
                    ModelState.AddModelError(string.Empty, "Cette page existe déjà.");
                    return View(applicationPage);
                }
                _context.Add(applicationPage);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { Message = PageMessageId.Create });
            }
            return View(applicationPage);
        }

        //
        // GET /Page/Edit/1
        [HttpGet]        
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            var model = await _context.Pages.SingleOrDefaultAsync(r => r.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            return View(model);
        }

        //
        // POST : /Page/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, Name, Content")] ApplicationPage applicationPage)
        {
            if (id != applicationPage.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicationPage);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationPageExists(applicationPage.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return RedirectToAction("Index", new { Message = PageMessageId.Edit });
        }

        //
        // GET : /Page/Delete/1
        [HttpGet]        
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationPage = await _context.Pages.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationPage == null)
            {
                return NotFound();
            }
            return View(applicationPage);
        }

        //
        // POST : //Page/Delete/1
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            var applicationPage = await _context.Pages.SingleOrDefaultAsync(m => m.Id == id);
            _context.Pages.Remove(applicationPage);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { Message = PageMessageId.Delete });
        }

        private bool ApplicationPageExists(int id)
        {
            return _context.Pages.Any(e => e.Id == id);
        }

        private bool ApplicationPageNameExists(string name)
        {
            return _context.Pages.Any(e => e.Name == name);
        }

        public enum PageMessageId
        {
            Create,
            Edit,
            Delete
        }
    }
}