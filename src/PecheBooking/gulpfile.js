﻿/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    clean = require('gulp-clean'),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    dotnet = require('gulp-dotnet');

var webroot = "./wwwroot/";

var paths = {
    js: webroot + "js/**/*.js",
    minJs: webroot + "js/**/*.min.js",
    css: webroot + "css/**/*.css",
    minCss: webroot + "css/**/*.min.css",
};

gulp.task("clean:js", function(cb) {
    return gulp.src(paths.minJs, { read: false })
        .pipe(clean());
});

gulp.task("clean:css", function(cb) {
    return gulp.src(paths.minCss, { read: false })
        .pipe(clean());
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function() {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function() {
    return gulp.src([paths.css, "!" + paths.minCss], { base: "." })
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);

gulp.task("build", ["clean", "min", "build:csharp"]);

gulp.task('build:csharp', function(cb) {
    dotnet.build({ cwd: './' }, cb);
});