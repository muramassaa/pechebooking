using System.Linq;
using System.Threading.Tasks;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class Page
    {
        public async Task<int> Seed(ApplicationDbContext context)
        {
            if (context.Pages.Count() == 0)
            {
                context.Pages.Add(new ApplicationPage { Name = "Réglement", Content = "Your rule page."});
            }
            return await context.SaveChangesAsync();
        }
    }
}