using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class Role
    {
        public async Task<int> Seed(ApplicationDbContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);

            if (!context.Roles.Any(r => r.Name == "administrator"))
            {
                await roleStore.CreateAsync(new IdentityRole { Name = "administrator", NormalizedName = "administrator" });
            }
            return await context.SaveChangesAsync();
        }
    }
}