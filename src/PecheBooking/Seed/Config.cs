using System.Linq;
using System.Threading.Tasks;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class Config
    {
        public async Task<int> Seed(ApplicationDbContext context)
        {
            if (!context.Configs.Any(r => r.Action == "Configuration")) 
            {
                context.Configs.Add(new ApplicationConfig { Id = "AppTitle", Action = "Configuration", Name = "Nom du site", Value = "PecheBooking" });
                context.Configs.Add(new ApplicationConfig { Id = "Address", Action = "Configuration", Name = "Numéro et libellé de la voie", Value = "Plan D'eau Du Pont Foulon" });
                context.Configs.Add(new ApplicationConfig { Id = "City", Action = "Configuration", Name = "Ville", Value = "Arrou" });
                context.Configs.Add(new ApplicationConfig { Id = "ZipCode", Action = "Configuration", Name = "Code postal", Value = "28290" });
                context.Configs.Add(new ApplicationConfig { Id = "Email", Action = "Configuration", Name = "Email du site", Value = "contact@pechebooking.fr" });
                context.Configs.Add(new ApplicationConfig { Id = "PhoneNumber", Action = "Configuration", Name = "Téléphone", Value = "" });
                context.Configs.Add(new ApplicationConfig { Id = "Copyright", Action = "Configuration", Name = "Nom de l'entreprise", Value = "PecheBooking" });
            }
            if (!context.Configs.Any(r => r.Action == "Social"))
            {
                context.Configs.Add(new ApplicationConfig { Id = "Facebook", Action = "Social", Name = "Facebook", Value = "" });
                context.Configs.Add(new ApplicationConfig { Id = "Instagram", Action = "Social", Name = "Instagram", Value = "" });
                context.Configs.Add(new ApplicationConfig { Id = "Youtube", Action = "Social", Name = "Youtube", Value = "" });
            }
            return await context.SaveChangesAsync();
        }
    }
}