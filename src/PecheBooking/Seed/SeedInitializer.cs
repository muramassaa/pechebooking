using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class SeedInitializer
    {
        private Config _config { get; set; }
        private Menu _menu { get; set; }
        private Page _page { get; set; }
        private Role _role { get; set; }
        private User _user { get; set; }
        private ApplicationDbContext context { get; set; }

        public SeedInitializer(
            Config config,
            Menu menu,
            Page page,
            Role role,            
            User user,
            ApplicationDbContext context)
        {
            this.context = context;
            _config = config;
            _menu = menu;
            _page = page;
            _role = role;
            _user = user;
        }

        public async void Seed()
        {
            await _config.Seed(context);
            await _menu.Seed(context);
            await _page.Seed(context);
            await _role.Seed(context);
            await _user.Seed(context);
        }
    }
}