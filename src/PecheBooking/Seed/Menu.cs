using System.Linq;
using System.Threading.Tasks;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class Menu
    {
        public async Task<int> Seed(ApplicationDbContext context)
        {
            if (context.Menus.Count() == 0)
            {
                context.Menus.Add(new ApplicationMenu { Id = 1, MenuText = "Accueil", LinkUrl = "/" });
                context.Menus.Add(new ApplicationMenu { Id = 2, MenuText = "Règlement", LinkUrl = "/Home/Page/Réglement" });
                context.Menus.Add(new ApplicationMenu { Id = 3, MenuText = "Prendre une réservation", LinkUrl = "/Reserve" });
            }
            return await context.SaveChangesAsync();
        }
    }
}