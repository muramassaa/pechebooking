using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PecheBooking.Areas.Admin.Models;
using PecheBooking.Data;

namespace PecheBooking.Seed
{
    public class User
    {
        public async Task<int> Seed(ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var user = new ApplicationUser
            {
                Email = "Admin@gmail.com",
                UserName = "Administrator",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                NormalizedUserName = "ADMINISTRATOR",
                SecurityStamp = Guid.NewGuid().ToString("D")
            };            
            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, "*");
                user.PasswordHash = hashed;
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "administrator");
            }
            return await context.SaveChangesAsync();
        }
    }
}